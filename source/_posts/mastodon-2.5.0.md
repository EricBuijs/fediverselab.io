---
layout: "post"
title: "Mastodon 2.5.0"
date: 2018-09-03
tags:
    - mastodon
preview: "Federation relays, public profiles and toots redesign, new interaction dialog for remote toots, public profile endorsements, and other additions"
url: "https://github.com/tootsuite/mastodon/releases/tag/v2.5.0"
lang: en
---

This release brings public profiles and toots redesign, a handy new dialog to interact with remote posts on any Mastodon page, public profile endorsements which one may use to recommend interesting users to follow, better resize for long images (like comics), a new admin dashboard with a summary of what's happening on the server, and many other improvements and fixes. Read full changelog [here](https://github.com/tootsuite/mastodon/releases/tag/v2.5.0). This release also features federation relays - an optional tool that helps new small instances get more content. A similar solution [exists](https://relay.iliketoast.net) for diaspora protocol networks.