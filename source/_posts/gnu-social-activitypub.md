
---
layout: "post"
title: "ActivityPub in GNU Social?"
date: 2018-04-05
tags:
    - gnusocial
preview: "User @dansup confirmed that ActivityPub support for GNU Social is in the works. They'll be working on the plugin this summer. Good news, Fediverse!"
url: "https://mastodon.social/@dansup/99808608417729060"
lang: en
---

User @dansup [confirmed](https://mastodon.social/@dansup/99808608417729060) that ActivityPub support for GNU Social is in the works. They'll be working on the plugin this summer. Good news for Fediverse!
