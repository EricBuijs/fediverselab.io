
---
layout: "post"
title: "postActiv 1.0.5. release"
date: 2018-07-30
tags:
    - postactiv
preview: "This stability and security update lays foundation for supporting more protocols in postActiv. Adding diaspora protocol is one of the goals"
url: "http://gitea.postactiv.com/postActiv/postActiv/releases"
lang: en
---

This release is mostly a stability and security update which lays the foundation for supporting more federated protocols in postActiv. Major changes include NodeInfo 2.0 implementation, scaffolding for adding multiple protocol support (DFRN, Zot, diaspora* protocols), security updates in extlibs, code formatting standardization.
Complete list of changes can be found in the [changelog](http://gitea.postactiv.com/postActiv/postActiv/releases) and commit logs.
